package com.reichart.cuby.canparser;

import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JFrame;

public class CanParser {
	
	private final static String VERSION = "1.2";

	public static void main(String[] args) {
		
		System.out.println("CAN-LogParser/Converter to CSV. Version: " + VERSION);
		System.out.println("USAGE:\n java -jar canparser.jar             : Loads standard file 'can.log'\n java -jar canparser.jar <FILENAME>  : Loads file <FILENAME>\n");
		
		String filename = null;
		if (args.length>0) {
			filename = args[0];
		} else {
			filename = "can.log";
		}
		
		CanParser parser = new CanParser();
		
		List<String> data = parser.loadFile(filename);
		
		System.out.println(String.format("[Debug] Loading Data from file %s - found %d lines", filename, data.size()));
		
		List<PDOInterface> tData = new ArrayList<>();

		for (String logline : data) {
			if (logline.contains("3]")) { // Z-MOTOR
				if (logline.contains(PDO1Data.getDesignator())) {
					tData.add(new PDO1Data(logline));
				} else if (logline.contains(PDO2Data.getDesignator())) {
					tData.add(new PDO2Data(logline));
				} else if (logline.contains(PDO3Data.getDesignator())) {
					tData.add(new PDO3Data(logline));
				} else if (logline.contains(PDO4Data.getDesignator())) {
					tData.add(new PDO4Data(logline));
				}
			}
		}
		
		final List<PDODataPoint> dataPoints = new ArrayList<>();
		
		final Date relativeStart = tData.get(0).getDate();
		final long relativeStartLong = relativeStart.getTime();

		System.out.println("*************** PDO1-Data ***************");
		for (PDOInterface aTgm : tData) {
			if (aTgm instanceof PDO1Data) {
				System.out.println(aTgm.getCSVRepresentation(relativeStart));
				dataPoints.add(new PDODataPoint(DataType.CURRENT_POS, (int) (aTgm.getDate().getTime()-relativeStartLong), ((PDO1Data) aTgm).getPosition()));
			}
		}
		System.out.println("*************** PDO2-Data ***************");
		for (PDOInterface aTgm : tData) {
			if (aTgm instanceof PDO2Data) {
				System.out.println(aTgm.getCSVRepresentation(relativeStart));
				dataPoints.add(new PDODataPoint(DataType.CURRENT_DEMAND_INT_VALUE, (int) (aTgm.getDate().getTime()-relativeStartLong), ((PDO2Data) aTgm).getCurrentDemandValue()));
				dataPoints.add(new PDODataPoint(DataType.BUS_VOLTAGE,              (int) (aTgm.getDate().getTime()-relativeStartLong), ((PDO2Data) aTgm).getBusVoltageValue()));
			}
		}
		System.out.println("*************** PDO3-Data ***************");
		for (PDOInterface aTgm : tData) {
			if (aTgm instanceof PDO3Data) {
				System.out.println(aTgm.getCSVRepresentation(relativeStart));
				final PDO3Data dat = (PDO3Data) aTgm;
//				dataPoints.add(new PDODataPoint(DataType.POSITION_DEMAND, (int) (dat.getDate().getTime()-relativeStartLong), dat.getPositionDemandValue()));
				dataPoints.add(new PDODataPoint(DataType.VELOCITY_DEMAND, (int) (dat.getDate().getTime()-relativeStartLong), dat.getVelocityDemandValue()));
				dataPoints.add(new PDODataPoint(DataType.ENCODER_DRIFT, (int) (dat.getDate().getTime()-relativeStartLong), dat.getEncoderDriftValue()));
			}
		}
		System.out.println("*************** PDO4-Data ***************");
		for (PDOInterface aTgm : tData) {
			if (aTgm instanceof PDO4Data) {
				System.out.println(aTgm.getCSVRepresentation(relativeStart));
				final PDO4Data dat = (PDO4Data) aTgm;
				dataPoints.add(new PDODataPoint(DataType.CURRENT_ACTUAL_VALUE, (int) (dat.getDate().getTime()-relativeStartLong), dat.getCurrent()));
				dataPoints.add(new PDODataPoint(DataType.VELOCITY_ACTUAL_VALUE, (int) (dat.getDate().getTime()-relativeStartLong), dat.getVelocity()));
			}
		}
		
		final JFrame frame = new JFrame("CAN Debugging Visualizer");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setPreferredSize(new Dimension(400, 300));
		frame.setSize(new Dimension(400, 300));
		
		
		final  GraphPanel panel = new GraphPanel(dataPoints);
		frame.setContentPane(panel);
		frame.invalidate();
		frame.repaint();
		
		panel.invalidate();
		panel.repaint();
		
		frame.setVisible(true);
	}

	public CanParser() {
		// Intentionally empty
	}

	public List<String> loadFile(String filename) {
		final Class<CanParser> clazz = CanParser.class;
		final InputStream inputStream = clazz.getClassLoader().getResourceAsStream(filename);
		List<String> data = new ArrayList<>();
		try {
			data = readFromInputStream(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
			return data;
		}

		return data;
	}

	private List<String> readFromInputStream(InputStream inputStream) throws IOException {
		final List<String> resultList = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
			String line;
			while ((line = br.readLine()) != null) {
				resultList.add(line);
			}
		}
		return resultList;
	}
}
