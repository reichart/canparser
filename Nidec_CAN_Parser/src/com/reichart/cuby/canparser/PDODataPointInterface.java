package com.reichart.cuby.canparser;

/**
 * Interface description for a PDO-DataPoint that is used to show a value at the correct position in the graphical view.
 * 
 * @author are
 *
 */
public interface PDODataPointInterface {

	public int getTime();
	public int getValue();
	public String getDesignator();
	public DataType getType();
	
}
