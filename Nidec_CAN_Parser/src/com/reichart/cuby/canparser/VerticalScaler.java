package com.reichart.cuby.canparser;

/**
 * Class that provides a scaling implementation to get a proper scaling in a graphical view. 
 * 
 * @author are
 *
 */
public class VerticalScaler {

	private float maxValue = Float.MIN_VALUE;
	private float minValue = Float.MAX_VALUE;
	private final float masterScaler;
	
	public VerticalScaler (float masterScaler) {
		this.masterScaler = masterScaler;
	}
	
	public void checkValue (float compareValue) {
		if (compareValue > maxValue) {
			maxValue = compareValue;
		}
		if (compareValue < minValue) {
			minValue = compareValue;
		}
	}

	public float getYFactor( int windowHeight) {
		return ( (((float)windowHeight) / getDelta() ) * masterScaler) * 0.5f;
	}
	
	public int getCenter(int windowHeight) {		
		return (int) ( getDelta() * (((float)windowHeight)/getDelta()  * 0.5f) );
	}
	
	private float getDelta() {
		return Math.abs(maxValue - minValue);
	}
	
}
