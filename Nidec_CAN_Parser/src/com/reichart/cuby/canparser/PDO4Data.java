package com.reichart.cuby.canparser;

import java.util.Date;

/**
 * Representation for PDO4 that implements the PDOInterface and extends the PDOData Class.
 * 
 * @author are
 *
 */
public class PDO4Data extends PDOData implements PDOInterface {
	
	private final int currentValue;
	private final int velocityValue;
	
	public PDO4Data(String logline) {
		super(logline);
		currentValue = getUnsigned32BitValue(dataString.substring(8));
		velocityValue = getVelocityValue(dataString.substring(0,8));
	}

	@Override
	public String getCSVRepresentation(Date relativeStart) {
		final StringBuilder builder = new StringBuilder();
		
		builder.append(dateFormatterOut.format(date));
		builder.append(SEPARATOR);
		builder.append(date.getTime() - relativeStart.getTime());
		builder.append(SEPARATOR);
		builder.append(currentValue);
		builder.append(SEPARATOR);
		builder.append(velocityValue);
		
		return builder.toString();
	}

	@Override
	public String getCSVHeader() {
		// TODO Auto-generated method stub
		return "";
	}
	
	private int getVelocityValue(String hexString) {
		String reversedString = hexString.substring(6);
		reversedString += hexString.substring(4, 6);
		reversedString += hexString.substring(2, 4);
		reversedString += hexString.substring(0, 2);
		
		return (int) Long.parseLong(reversedString, 16);
	}

	public static String getDesignator() {
		return "[48";
	}
	
	public int getVelocity() {
		return velocityValue;
	}
	
	public int getCurrent() {
		return currentValue;
	}

}
