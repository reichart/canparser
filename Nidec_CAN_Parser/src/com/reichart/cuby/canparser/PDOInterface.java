package com.reichart.cuby.canparser;

import java.util.Date;

/**
 * Interface description what a PDO implementation for a logline shall provide.
 * 
 * @author are
 *
 */
public interface PDOInterface {

	public String getCSVRepresentation(Date relativeStart);

	public String getCSVHeader();

	public Date getDate();

}
