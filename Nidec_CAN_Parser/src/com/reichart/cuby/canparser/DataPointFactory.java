package com.reichart.cuby.canparser;

/**
 * Factory to provide matching scaling factors for PDO-Datapoint Types.
 * @author are
 *
 */
public class DataPointFactory {
	
	public DataType getDataPoint(DataType type) {
    	if (type == DataType.CURRENT_POS || type == DataType.POSITION_DEMAND) {
    		return DataType.CURRENT_POS;
    	} else if (type == DataType.VELOCITY_ACTUAL_VALUE || type == DataType.VELOCITY_DEMAND) {
    		return DataType.VELOCITY_ACTUAL_VALUE;
    	} else if (type == DataType.CURRENT_ACTUAL_VALUE || type == DataType.CURRENT_DEMAND_INT_VALUE ){
    		return DataType.CURRENT_ACTUAL_VALUE;
    	} else { 
    		return type;
    	}
	}
}
