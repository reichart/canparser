package com.reichart.cuby.canparser;

import java.util.Date;

/**
 * Representation for PDO1 that implements the PDOInterface and extends the PDOData Class.
 * 
 * @author are
 *
 */
public class PDO1Data extends PDOData implements PDOInterface {
	private String statusHex;
	private int position;
	
	public PDO1Data(String logline) {
		super(logline);
		
		String hexString = dataString.substring(6).trim();
		position = getSigned32BitValue(hexString);
		statusHex = getStatusHex(dataString);
	}
	
	public String getCSVRepresentation() {
		StringBuilder builder = new StringBuilder();
		builder.append(dateFormatterOut.format(date));
		builder.append(", ");
		builder.append(position);
		return builder.toString();
	}
	
	public String getCSVHeader() {
		return 	"Date, DeltaTime[ms], Position[Ticks], StatusWord[hex], StatusWord[Bit 0 -> 15], Flags";
	}
	
	
	public String getCSVRepresentation(Date relativeStart) {
		
		int statusInt = Integer.parseUnsignedInt(statusHex, 16);
		
		StringBuilder builder = new StringBuilder();		
		
		builder.append(dateFormatterOut.format(date));
		builder.append(", ");
		builder.append(date.getTime() - relativeStart.getTime());
		builder.append(", ");
		builder.append(position);
		builder.append(", ");
		builder.append(statusHex);
		builder.append(", ");
		builder.append(getHRStatusString(Integer.parseUnsignedInt(statusHex, 16)));
		builder.append(",");
		if ((statusInt & (1<<3)) != 0) {
			builder.append("FAULT");
		}
		builder.append(",");
		if ((statusInt & (1<<10)) != 0) {
			builder.append("TARGET_REACHED");
		}
		builder.append(",");
		if ((statusInt & (1<<12)) != 0) {
			builder.append("SETPOINT");
		}
		builder.append(",");
		if((statusInt & (1<<5)) != 0) {
			builder.append("QUICKSTOP");
		}
		
		return builder.toString();
	}
	
	public String getHRStatusString(int statusInt) {
		return getHumanReadableBitString(getStatusInt(), 16);
	}
	
	public int getStatusInt() {
		return Integer.valueOf(statusHex, 16);
	}
	
	private String getStatusHex(String hexString) {
		String returnString = hexString.substring(2, 4);
		returnString += hexString.substring(0, 2);
		return returnString;
	}
	
	private String getHumanReadableBitString (int value, int bitlength){
	    String return_string = "";
	    for (int i = 0; i<bitlength; ++i){
	        if (i%4==0 && i!=0){
	            return_string += " ";    // add a separator to make it more readable
	        }
	        return_string += ( ((value >> i) & 0x01) == 0x01) ? "1" : "0"; // costly!
	    }
	    return return_string;
	}

	public int getPosition() {
		return position;
	}

	public String getStatusHex() {
		return statusHex;
	}

	public static String getDesignator() {
		return "[18";
	}
}