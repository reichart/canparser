package com.reichart.cuby.canparser;

import java.util.Date;

/**
 * Representation for PDO3 that implements the PDOInterface and extends the PDOData Class.
 * 
 * @author are
 *
 */
public class PDO3Data extends PDOData implements PDOInterface {
	
	private int positionDemandValue;
	private final int velocityDemandValue;
	private final int encoderDriftValue;
	
	public PDO3Data(String logline) {
		super(logline);
//		positionDemandValue = getSigned32BitValue(dataString.substring(0,8));
		encoderDriftValue = getSigned16BitValue(dataString.substring(0,4));
		velocityDemandValue = getSigned32BitValue(dataString.substring(4));
	}

	@Override
	public String getCSVRepresentation(Date relativeStart) {
		final StringBuilder builder = new StringBuilder();
		
		builder.append(dateFormatterOut.format(date));
		builder.append(SEPARATOR);
		builder.append(date.getTime() - relativeStart.getTime());
		builder.append(SEPARATOR);
//		builder.append(positionDemandValue);
		builder.append(encoderDriftValue);
		builder.append(SEPARATOR);
		builder.append(velocityDemandValue);
		
		return builder.toString();
	}

	@Override
	public String getCSVHeader() {
		// TODO Auto-generated method stub
		return "";
	}

	public static String getDesignator() {
		return "[38";
	}
	
	public int getPositionDemandValue() {
		return positionDemandValue;
	}
	
	public int getEncoderDriftValue() {
		return encoderDriftValue;
	}
	
	public int getVelocityDemandValue() {
		return velocityDemandValue;
	}
}
