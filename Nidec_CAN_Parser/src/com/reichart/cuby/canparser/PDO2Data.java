package com.reichart.cuby.canparser;

import java.util.Date;

/**
 * Representation for PDO2 that implements the PDOInterface and extends the PDOData Class.
 * 
 * @author are
 *
 */
public class PDO2Data extends PDOData implements PDOInterface{

	private int currentDemandValue = 0;
	private int busVoltage = 0;
	
	public PDO2Data(String logline) {
		super(logline);
		currentDemandValue = getUnsigned32BitValue(dataString.substring(0, 8));
		busVoltage = getSigned32BitValue(dataString.substring(8));
	}

	@Override
	public String getCSVRepresentation(Date relativeStart) {
		final StringBuilder builder = new StringBuilder();
		
		builder.append(dateFormatterOut.format(date));
		builder.append(SEPARATOR);
		builder.append(date.getTime() - relativeStart.getTime());
		builder.append(SEPARATOR);
		builder.append(currentDemandValue);
		builder.append(SEPARATOR);
		builder.append(busVoltage);
		
		return builder.toString();
	}

	@Override
	public String getCSVHeader() {
		return "Data, RelativeTime, CurrentDemandValue (0x2005/00), BusVoltageValue (0x6079/00)";
	}

	public static String getDesignator() {
		return "[28";
	}
	
	public int getCurrentDemandValue() {
		return currentDemandValue;
	}
	
	public int getBusVoltageValue() {
		return busVoltage;
	}

}
