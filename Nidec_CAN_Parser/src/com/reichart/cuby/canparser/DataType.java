package com.reichart.cuby.canparser;

import java.awt.Color;

/**
 * Enumeration for the objects that are contained within PDOs.
 * @author are
 *
 */
public enum DataType {

	CURRENT_POS(Color.BLUE, "Current Position 0x6064"), 
	POSITION_DEMAND(Color.ORANGE, "Position Demand 0x6062"), 
	VELOCITY_DEMAND(Color.ORANGE, "Velocity Demand 0x206B"), 
	CURRENT_ACTUAL_VALUE(Color.RED, "Current Actual Value 0x2045"),
	VELOCITY_ACTUAL_VALUE(Color.GREEN, "Velocity Actual Value 0x606C"), 
	CURRENT_DEMAND_INT_VALUE(Color.DARK_GRAY, "Current Demand Internal Value 0x2005"), 
	BUS_VOLTAGE(Color.PINK, "Bus Voltage 0x6079"), 
	ENCODER_DRIFT(Color.BLACK, "EncoderDrift Value, 0x2604");

	private final Color color;
	private final String name;

	private DataType(Color color, String name) {
		this.color = color;
		this.name = name;
	}

	public Color getColor() {
		return this.color;
	}
	
	@Override
	public String toString() {
		return this.name;
	}

}
