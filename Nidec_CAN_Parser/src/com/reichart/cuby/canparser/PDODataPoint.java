package com.reichart.cuby.canparser;

import java.awt.Color;

/**
 * Implementation of the {@linkplain PDODataPointInterface} description that may
 * be either used as the base class for special implementations or act as a
 * usable DataPoint.
 * 
 * @author are
 *
 */
public class PDODataPoint implements PDODataPointInterface {
	protected final DataType type;
	protected int time;
	protected int value;

	public PDODataPoint(DataType type, String designator) {
		this.type = type;
		this.time = 0;
		this.value = 0;
	}

	public PDODataPoint(DataType type, int time, int value) {
		this.type = type;
		this.time = time;
		this.value = value;
	}

	@Override
	public int getTime() {
		return time;
	}

	@Override
	public int getValue() {
		return value;
	}

	@Override
	public String getDesignator() {
		return type.toString();
	}

	@Override
	public DataType getType() {
		return type;
	}

	public Color getColor() {
		return type.getColor();
	}
}
