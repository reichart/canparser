package com.reichart.cuby.canparser;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Base Class for PDO-Data Telegrams.
 * 
 * @author are
 *
 */
public class PDOData {

	protected Date date;
	protected static SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	protected static SimpleDateFormat dateFormatterOut = new SimpleDateFormat("HH:mm:ss.SSS");
	
	protected String dataString;

	protected final static String SEPARATOR = ", ";
	
	public PDOData(String logline) {
		String dateString = logline.substring(0, 24).trim();
		
		int indexStart = logline.indexOf("{");
		int indexEnd = logline.indexOf("}");
		dataString = logline.substring(indexStart+1, indexEnd).trim();
		
		try {
			date = dateFormatter.parse(dateString.replace('T', ' '));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public Date getDate() {
		return date;
	}
	
	public int getRelativeTime(Date date) {
		return (int) (this.date.getTime() - date.getTime());
	}
	
	protected int getSigned32BitValue(String hexString) {
		String reversedString = hexString.substring(6);
		reversedString += hexString.substring(4, 6);
		reversedString += hexString.substring(2, 4);
		reversedString += hexString.substring(0, 2);
		
		return (int) Long.parseLong(reversedString, 16);
	}
	
	protected int getSigned16BitValue(String hexString) {
		String reversedString = hexString.substring(2);
		reversedString += hexString.substring(0, 2);
		
		return (int) Integer.valueOf(reversedString, 16).shortValue();
	}
	
	protected int getUnsigned32BitValue(String hexString) {
		String reversedString = hexString.substring(6);
		reversedString += hexString.substring(4, 6);
		reversedString += hexString.substring(2, 4);
		reversedString += hexString.substring(0, 2);
		return new BigInteger(reversedString, 16).intValue();
	}
}