package com.reichart.cuby.canparser;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

/**
 * Simple graphical view for the PDO Data.
 * 
 * Currently only the 
 * 
 * @author are
 *
 */
public class GraphPanel extends JPanel {
	
	private static final long serialVersionUID = -5370841255707564075L;
	private final List<PDODataPoint>dataPoints;
    final static float SCALE_FACTOR = 0.95f;
	
	public GraphPanel(List<PDODataPoint> data) {
		this.dataPoints=data;
	}
	
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        final Graphics2D g2 = (Graphics2D)g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
	    g2.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
	    g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        
        final int x=getWidth();
        final int y=getHeight(); 
        
        final Map<DataType, VerticalScaler> scalers = new HashMap<DataType, VerticalScaler>();
        scalers.put(DataType.CURRENT_POS, new VerticalScaler(SCALE_FACTOR));
        scalers.put(DataType.POSITION_DEMAND, new VerticalScaler(SCALE_FACTOR));
        scalers.put(DataType.VELOCITY_DEMAND, new VerticalScaler(SCALE_FACTOR));
        scalers.put(DataType.VELOCITY_ACTUAL_VALUE, new VerticalScaler(SCALE_FACTOR));
        scalers.put(DataType.CURRENT_ACTUAL_VALUE, new VerticalScaler(SCALE_FACTOR));
        scalers.put(DataType.BUS_VOLTAGE, new VerticalScaler(0.5f));
        scalers.put(DataType.CURRENT_DEMAND_INT_VALUE, new VerticalScaler(SCALE_FACTOR));
        scalers.put(DataType.ENCODER_DRIFT, new VerticalScaler(SCALE_FACTOR));
        
        final DataPointFactory factory = new DataPointFactory();

        for (PDODataPoint dPoint : dataPoints) {
        	VerticalScaler scaler = scalers.get(factory.getDataPoint(dPoint.getType()));
        	scaler.checkValue(dPoint.getValue());
        }
        
        int textYPos = 20;
        for (Map.Entry<DataType, VerticalScaler> entry : scalers.entrySet()) {
        	g2.setColor(entry.getKey().getColor());
        	g2.drawString(entry.getKey().toString(), 200, textYPos);
        	textYPos += 15;
        }
                   
        final float maxX=dataPoints.parallelStream().max(Comparator.comparing(p->p.getTime())).get().getTime();
        final float xFactor=((float)x)/((float)maxX) * 0.98f;
        
        g2.setColor(Color.BLACK);
        g2.drawLine(0, scalers.get(DataType.CURRENT_POS).getCenter(y), (int)maxX, scalers.get(DataType.CURRENT_POS).getCenter(y));
              
        for (PDODataPoint point : dataPoints) {
        	final VerticalScaler scaler = scalers.get(factory.getDataPoint(point.getType()));
        	final int xPoint = (int) (xFactor * (float)point.getTime());
        	final int yPoint = scaler.getCenter(y) - (int) (scaler.getYFactor(y) *(float)point.getValue());
        	g2.setColor(point.getColor());
        	g2.drawRect(xPoint-1, yPoint-1, 2, 2);
        	
        }
	}
}
